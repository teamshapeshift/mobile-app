import 'package:flutter/material.dart';
import 'package:vector_math/vector_math_64.dart';

import 'package:shapeshifter_app/utils/graphics.dart';

class PreviewCube {

  double _index;
  double _size = 2;

  PreviewCube(this._index, {x : 0, y : 0, z : 0}) {
    var s = _size / 2;

    x = (x != 0 ? _size : 0) + x;
    z = (z != 0 ? _size : 0) + z;
    y = (y != 0 ? y / 3 : -2);

    
    _vertices = [
      Vector3(-s - x, -s, -s + z), // rechts-achter-onder
      Vector3(s - x, -s, -s + z), // links-achter-onder
      Vector3(s - x, s + y, -s + z), // links-achter-boven
      Vector3(-s - x, s + y, -s + z), // rechts-achter-boven
      Vector3(-s - x, -s, s + z), // rechts-voren-onder
      Vector3(s - x, -s, s + z), // links-voren-onder
      Vector3(s - x, s + y, s + z), // links-voren-boven
      Vector3(-s - x, s + y, s + z), // rechts-voren-boven
    ];

    _edges = [
      Vector2(_index * 8 + 0, _index * 8 + 1),
      Vector2(_index * 8 + 1, _index * 8 + 2),
      Vector2(_index * 8 + 2, _index * 8 + 3),
      Vector2(_index * 8 + 3, _index * 8 + 0),
      Vector2(_index * 8 + 4, _index * 8 + 5),
      Vector2(_index * 8 + 5, _index * 8 + 6),
      Vector2(_index * 8 + 6, _index * 8 + 7),
      Vector2(_index * 8 + 7, _index * 8 + 4),
      Vector2(_index * 8 + 0, _index * 8 + 4),
      Vector2(_index * 8 + 1, _index * 8 + 5),
      Vector2(_index * 8 + 2, _index * 8 + 6),
      Vector2(_index * 8 + 3, _index * 8 + 7),
    ];
  }

  List<Vector3> _vertices;
  List<Vector2> _edges;

  List<Vector3> get vertices => _vertices;
  List<Vector2> get edges => _edges;
}