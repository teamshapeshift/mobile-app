  import 'package:flutter/material.dart';

import 'package:shapeshifter_app/components/preview_cube.dart';
import 'package:shapeshifter_app/utils/graphics.dart';
import 'package:vector_math/vector_math_64.dart' as vmath;

class PreviewPainter extends CustomPainter {
  Paint linePaint;
  List<double> _heights;

  double _dx;
  double _dy;

  PreviewPainter(this._dx, this._dy, this._heights) {
    linePaint = Paint()
        ..color = Colors.black
        ..strokeWidth = 2;
  }

  @override
  void paint(Canvas canvas, Size size) {
    canvas.drawRect(Offset.zero & size, Paint()..color=Colors.white);

    var cubes = <PreviewCube>[
      PreviewCube(0, y: _heights[0]),
      PreviewCube(1, y: _heights[1], x: .5),
      PreviewCube(2, y: _heights[2], z: .5),
      PreviewCube(3, y: _heights[3], x: .5, z: .5),
    ];

    _drawCubes(canvas, cubes);
  }

  void _drawCubes(Canvas canvas, List<PreviewCube> cubes) {
    var edges = cubes
        .map((e) => e.edges)
        .expand((x) => x)
        .toList();

    var vertices = cubes
        .map((e) => e.vertices)
        .expand((x) => x)
        .toList();

    for (var edge in edges) {
      List<Offset> points = [];

      for (var v in [vertices[edge.x.toInt()], vertices[edge.y.toInt()]]) {
        var localV = v.clone();

        // Move the origin
        localV.add(vmath.Vector3(1.25, 0, -1.25));

        // Apply rotations
        localV.applyMatrix3(Graphics.rotateY(_dx));
        localV.applyMatrix3(Graphics.rotateX(_dy));
        localV.applyMatrix3(Graphics.rotateZ(0));

        // Move the rear to the back
        localV.add(vmath.Vector3(0, 0, -5));

        // Apply perspective
        var localV4 = vmath.Vector4(localV.x, localV.y, localV.z, 1);
        localV4.applyMatrix4(Graphics.perspectiveMatrix(-2));
        localV = localV4.xyz / localV4.z;

        // Scale the model
        localV.addScaled(localV, 100);

        // Move the model to a certain position to draw
        localV.add(vmath.Vector3(200, 300, 0));

        points.add(Offset(localV.x, localV.y));
      }
      canvas.drawLine(points[0], points[1], linePaint);
    }
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}