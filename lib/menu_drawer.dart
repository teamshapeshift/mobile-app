import 'package:flutter/material.dart';

class MenuDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Drawer(
      child: ListView(
        children: <Widget>[
          UserAccountsDrawerHeader(
            accountName: Text('Team Shapeshifter'),
            accountEmail: Text('Project 78'),
          ),
          ListTile(
            title: Text('Design'),
            trailing: Icon(Icons.brush),
            onTap: () => Navigator.pushReplacementNamed(context, '/design'),
          ),
          SizedBox(
            height: 1.0,
            child: Center(
              child: Container(
                margin: EdgeInsetsDirectional.only(start: 1.0, end: 1.0),
                height: 5.0,
                color: Colors.grey,
              ),
            ),
          ),
          ListTile(
            title: Text('Premade'),
            trailing: Icon(Icons.details),
            onTap: () => Navigator.pushNamed(context, '/premade'),
          ),
          SizedBox(
            height: 1.0,
            child: Center(
              child: Container(
                margin: EdgeInsetsDirectional.only(start: 1.0, end: 1.0),
                height: 5.0,
                color: Colors.grey,
              ),
            ),
          ),
          ListTile(
            title: Text('About'),
            trailing: Icon(Icons.info_outline),
            onTap: () => Navigator.pushNamed(context, '/about'),
          ),
        ],
      ),
    );
  }

}