class IntProperty {
  int value;

  IntProperty({this.value: 0});

  @override
  String toString() {
    return value.toString();
  }
}
