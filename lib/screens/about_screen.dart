import 'package:flutter/material.dart';
import 'package:chewie/chewie.dart';
import 'package:video_player/video_player.dart';

class AboutScreen extends StatefulWidget {
  final VideoPlayerController videoPlayerController;
  final bool looping;

  const AboutScreen({
    Key key,
    this.videoPlayerController,
    this.looping
  }) : super(key: key);

  @override
  _AboutScreenState createState() => _AboutScreenState();
}

class _AboutScreenState extends State<AboutScreen>{
  ChewieController _chewieController;
  @override
  void initState()
  {
    super.initState();
    _chewieController = ChewieController(
      videoPlayerController: widget.videoPlayerController,
      aspectRatio: 16 / 9,
      autoInitialize: true, // to see the first frame of the video
      looping: widget.looping,
      errorBuilder: (context, errorMessage) {
        return Center(
          child: Text(
            errorMessage,
            style: TextStyle(color: Colors.white),
          ),
        );
      },
    );
  }
  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
       // leading: Icon(Icons.apps),
        title: Text('About'),
      ),
      body: Column(
        children: <Widget>[
          Container(
            margin: const EdgeInsets.all(20.0),
            child: Text(
              'This project was started in response to the wish of the educational '+
              'manager at the Rotterdam University of Applied Science, Erik van Dijk.' +
              'Project Shapeshifter is a machine that can render something physically. ' +
              'This app will help with this, so the end-user can manipulate the Shapeshifter. '+
              'TEXT......',
              textAlign: TextAlign.left,
              overflow: TextOverflow.ellipsis,
              maxLines: 10,
            ),
          ),
          Container(

            margin: const EdgeInsets.all(20.0),
            child: Chewie(
              controller: _chewieController,
            ),
          )
        ],
      )
    );
  }
  @override
  void dispose() {
    super.dispose();
    widget.videoPlayerController.dispose();
    _chewieController.dispose();
  }
}
