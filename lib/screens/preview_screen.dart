import 'package:flutter/material.dart';
import 'package:shapeshifter_app/components/preview_painter.dart';

class PreviewScreen extends StatefulWidget {
  final String title;
  final List<double> _heights;

  PreviewScreen(this.title, this._heights);

  @override
  State<PreviewScreen> createState() => _PreviewScreenState(title, _heights);
}

class _PreviewScreenState extends State<PreviewScreen> {
  final String title;
  final List<double> _heights;

  _PreviewScreenState(this.title, this._heights);

  double _x = 0;
  double _dx = 0;

  double _y = 0;
  double _dy = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
      ),
      body: GestureDetector(
        onHorizontalDragStart: (details) => _x = details.globalPosition.dx,
        onHorizontalDragUpdate: (details) {
          setState(() {
            _dx += details.globalPosition.dx - _x;
            _x = details.globalPosition.dx;
          });
        },
        onVerticalDragStart: (details) => _y = details.globalPosition.dy,
        onVerticalDragUpdate: (details) {
          setState(() {
            _dy -= details.globalPosition.dy - _y;
            _y = details.globalPosition.dy;
          });
        },
        child: CustomPaint(
          painter: PreviewPainter(_dx, _dy, _heights),
          child: Container(),
        ),
      ),
    );
  }
}
