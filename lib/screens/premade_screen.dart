

import 'package:flutter/material.dart';
import 'package:shapeshifter_app/property.dart';


class PremadeScreen extends StatelessWidget {
  final String title;
  PremadeScreen(this.title);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
      ),
      body: ListView(
        children: <Widget> [
          ListTile(
            leading: Icon(Icons.view_comfy),
            title: Text('Cube'),
            onTap: () {
              List<IntProperty> heights = new List.filled(4, IntProperty(value: 5));
              Navigator.pushReplacementNamed(context, '/design', arguments: heights);
            },
          ),
        ]
      ),
    );
  }
}
