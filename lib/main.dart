import 'dart:convert';
import 'dart:io';


import 'package:flutter/material.dart';
import 'package:shapeshifter_app/screens/about_screen.dart';
import 'package:shapeshifter_app/menu_drawer.dart';
import 'package:shapeshifter_app/screens/preview_screen.dart';
import 'package:shapeshifter_app/screens/premade_screen.dart';
import 'package:video_player/video_player.dart';
import 'grid_item.dart';
import 'property.dart';

void main() => runApp(
  MaterialApp(
    theme: new ThemeData(
        primarySwatch: Colors.amber
    ),
    home: MainApp(),
    onGenerateRoute: _getRoute,
));

Route<dynamic> _getRoute(RouteSettings settings) {
  if (settings.name == '/design') {
    return _buildRoute(settings, MainApp(settings.arguments));
  }

  if (settings.name == '/premade') {
    return _buildRoute(settings, PremadeScreen('Premade'));
  }

  if (settings.name == '/preview') {
    return _buildRoute(settings, PreviewScreen('Preview', settings.arguments));
  }

  if (settings.name == '/about') {
    return _buildRoute(settings, AboutScreen(
      videoPlayerController: VideoPlayerController.asset(
        'videos/1507940147251-drlcss.mp4',
      ),
      looping: true,
    ));
  }

  return null;
}

MaterialPageRoute _buildRoute(RouteSettings settings, Widget builder) {
  return new MaterialPageRoute(
    settings: settings,
    builder: (ctx) => builder,
  );
}

class MainApp extends StatefulWidget {
  List<IntProperty> _predefinedHeights;

  MainApp([this._predefinedHeights]);

  @override
  _MainAppState createState() => _MainAppState(this._predefinedHeights);
}

class _MainAppState extends State<MainApp> {
  static const String ip = '192.168.43.179';

  int _gridSize;
  IntProperty _heightProperty;
  List<IntProperty> _predefinedHeights;
  Function(int) _updateItem;
  List<GridItem> _items;

  _MainAppState([this._predefinedHeights]) {
    _gridSize = 2;
    _items = List<GridItem>.generate(
      _gridSize * _gridSize,
      (int index) => GridItem(this._predefinedHeights != null ? this._predefinedHeights[index] : IntProperty(), _onButtonClick)
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
       // leading: Icon(Icons.apps),
        title: Text('Shapeshift'),
      ),
      drawer: MenuDrawer(),
      body: Column(
        children: <Widget>[
          Expanded(
            child: GridView.count(
              crossAxisCount: _gridSize,
              childAspectRatio: 1.0,
              padding: EdgeInsets.all(20.0),
              mainAxisSpacing: 10.0,
              crossAxisSpacing: 10.0,
              children: _items,
            )
          ),
          Container(
            height: 60,
            padding: EdgeInsets.all(16.0),
            child: Row(
              children: <Widget> [
                Flexible(
                  flex: 1,
                  child: Slider(
                    min: 0.0,
                    max: 10.0,
                    onChanged: _onSliderChange,
                    value: _heightProperty?.value?.toDouble() ?? 0
                  ),
                ),
                Container(
                  width: 40.0,
                  alignment: Alignment.center,
                  child: Text(
                    '${_heightProperty?.value ?? 0}',
                    style: TextStyle(fontSize: 20)
                  ),
                )
              ]
            ),
          ),
          Container(
            height: 60,
            color: Colors.white,
            child: ButtonTheme(
              minWidth: double.infinity,
              child: FlatButton(
                child: Text(
                  'Preview',
                  style: TextStyle(fontSize: 18),
                ),
                onPressed: () => Navigator.pushNamed(context, '/preview', arguments: _items.map((it) => it.heightProperty.value.toDouble()).toList())
              ),
            ),
          ),
          Container(
            height: 60,
            color: Colors.white,
            child: ButtonTheme(
              minWidth: double.infinity,
              child: FlatButton(
                child: Text(
                  'Send',
                  style: TextStyle(fontSize: 18),
                ),
                onPressed: _onSend,
              ),
            ),
          ),
        ],
      ),
    );
  }

  void _onButtonClick(IntProperty heightProperty, Function(int) updateFunc) {
    setState(() {
      _heightProperty = heightProperty;
      _updateItem = updateFunc;
    });
  }

  void _onSliderChange(double value) {
    int height = value.toInt();
    _updateItem(height);
    setState(() => _heightProperty.value = height);
  }

  void _onSend() async {
    String heightMap = _items
                        .map((it) => it.heightProperty)
                        .join(', ');

    print('Sending the data `$heightMap` to `$ip`');

    Socket socket = await Socket.connect(ip, 30001);
    socket.add(utf8.encode(heightMap));

    await Future.delayed(Duration(seconds: 1));

    socket.close();
  }
}
