import 'package:flutter/material.dart';

import 'property.dart';

class GridItem extends StatefulWidget {
  final IntProperty heightProperty;
  final Function onClickEvent;

  GridItem(this.heightProperty, this.onClickEvent);

  @override
  _GridItemState createState() => _GridItemState(this.heightProperty, this.onClickEvent);
}

class _GridItemState extends State<GridItem> {
  final IntProperty heightProperty;
  final Function onClickEvent;

  _GridItemState(this.heightProperty, this.onClickEvent);

  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      child: Text(
        '${heightProperty.value}',
        style: TextStyle(fontSize: 46),
      ),
      onPressed: () => onClickEvent(heightProperty, _updateHeight),
    );
  }

  void _updateHeight(int height) {
    setState(() => this.heightProperty.value = height);
  }
}
