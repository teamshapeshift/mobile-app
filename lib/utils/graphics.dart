import 'dart:math';

import 'package:vector_math/vector_math_64.dart';

class Graphics {
  static Matrix4 perspectiveMatrix(double zp) {
    return Matrix4(
      zp, 0, 0, 0,
      0, zp, 0, 0,
      0, 0, zp, 0,
      0, 0, 1, 0
    );
  }

  static Matrix3 rotateY(double degrees) {
    var theta = radians(degrees);
    var cosTheta = cos(theta);
    var sinTheta = sin(theta);

    return Matrix3(
      cosTheta, 0 ,sinTheta,
      0, 1, 0,
      -sinTheta, 0, cosTheta
    );
  }

  static Matrix3 rotateX(double degrees) {
    var theta = radians(degrees);
    var cosTheta = cos(theta);
    var sinTheta = sin(theta);

    return Matrix3(
      1, 0 ,0,
      0, cosTheta, -sinTheta,
      0, sinTheta, cosTheta
    );
  }

  static Matrix3 rotateZ(double degrees) {
    var theta = radians(degrees);
    var cosTheta = cos(theta);
    var sinTheta = sin(theta);

    return Matrix3(
      cosTheta, -sinTheta ,0,
      sinTheta, cosTheta, 0,
      0, 0, 1
    );
  }
}